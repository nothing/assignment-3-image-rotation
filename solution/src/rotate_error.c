#ifndef ROTATE_ERROR_C
#define ROTATE_ERROR_C
#include "include/image.h"
#include "include/rotate.h"
#include "include/rotate_error.h"
#include <stdlib.h>

enum rotate_status rotate_by_angle(int64_t angle, struct image* img, struct image* new_img ){
    if(angle%90 == 0) {
        switch (angle) {
            case 0:
                *new_img = rotate_0(img, new_img);
                break;
            case 90:
            case -270:
                *new_img = rotate_90(img, new_img);
                break;
            case 180:
            case -180:
                *new_img = rotate_180(img, new_img);
                break;
            case 270:
            case -90:
                *new_img = rotate_270(img, new_img);
                break;
            default:
                free_image(new_img);
                free_image(img);
                exit(1);
        }
        printf("%s", rotate_errors[0]);
        return ROTATE_OK;
    }else{
        printf("%s", rotate_errors[1]);
        free_image(new_img);
        free_image(img);
        exit(1);
    }
}

#endif//ROTATE_ERROR_C
