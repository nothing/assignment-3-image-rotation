#include "include/header.h"
#include "include/image.h"
#include "include/input_output.h"
#include "include/open_stream_error.h"
#include "stdlib.h"
#include <stdio.h>

#ifndef IMAGE_C
#define IMAGE_C

struct image* load_image(const char* file_name){
    FILE* fp = fopen(file_name, "rb");
    open_stream(fp);
    struct image* img = (struct image*) malloc(sizeof(struct image));
    uint8_t read_status= from_bmp(fp, img);
    fclose(fp);
    if(read_status != 0 ){
        if(read_status <= 3){
            free(img);
        }else{
            free(img->data);
            free(img);
        }
        exit(1);
    }
    return img;
}

int write_image(struct image const source, const char* file_name){
    FILE* fp = fopen(file_name, "wb");
    struct bmp_header header;
    uint8_t write_status = to_bmp(fp, &source, &header);
    fclose(fp);
    if(write_status != 0){
        printf("%s", writing_errors[1]);
        return WRITE_ERROR;
    }
    printf("%s", writing_errors[0]);
    return WRITE_OK;
}

void free_image(struct image* img){
    if(img){
        free(img->data);
        img->data = NULL;
        free(img);
        img = NULL;
    }
}
#endif //IMAGE_C
