#ifndef ROTATE_C
#define ROTATE_C
#include "include/image.h"
#include "include/rotate_error.h"
#include "stdlib.h"

struct image rotate_0( struct image* const source, struct image* new){
    new->data = malloc((source->height * sizeof(struct pixel) * source->width));
    new->width = source->width;
    new->height = source->height;
    size_t counter = 0;
    for(size_t i = 0; i < source->width * source->height; ++i){
        new->data[counter] = source->data[counter];
        counter++;
    }
    return *new;
}

struct image rotate_90( struct image* const source, struct image* new){
    new->data = malloc((source->height * sizeof(struct pixel) * source->width));
    new->width = source->height;
    new->height = source->width;
    size_t counter = 0;
    for(size_t i = 1; i <= source->width; ++i){
        for(size_t g = 1; g <= source->height; ++g){
            size_t index = source->width * g - i;
            new->data[counter] = source->data[index];
            counter++;
        }
    }
    return *new;
}

struct image rotate_180( struct image* source, struct image* new){
    new->data = malloc((source->height * sizeof(struct pixel) * source->width));
    new->width = source->width;
    new->height = source->height;
    size_t counter = 0;
    for(size_t i = 0; i < source->height; ++i){
        for(size_t g = 1; g <= source->width; ++g){
            size_t index = source->width * (source->height - i) - g;
            new->data[counter] = source->data[index];
            counter++;
        }
    }
    return *new;
}

struct image rotate_270( struct image* source, struct image* new){
    new->data = malloc((source->height * sizeof(struct pixel) * source->width));
    new->width = source->height;
    new->height = source->width;
    size_t counter = 0;
    for(size_t i = 0; i < source->width; ++i){
        for(size_t g = 1; g <= source->height; ++g){
            size_t index = source->width * (source->height - g) + i;
            new->data[counter] = source->data[index];
            counter++;
        }
    }
    return *new;
}

struct image* rotate_on_angle(struct image* const source, int64_t angle){
    struct image *new_img = malloc(sizeof(struct image));
    rotate_by_angle(angle, source, new_img);
    return new_img;
}

#endif//ROTATE_C
