#ifndef OPEN_STREAM_ERROR_C
#define OPEN_STREAM_ERROR_C
#include "include/open_stream_error.h"
#include "stdio.h"
#include <stdlib.h>

enum open_stream_status open_stream( FILE* fp){
    if(fp == NULL){
        printf("%s", open_stream_errors[1]);
        exit(1);
    }
    return OPEN_OK;
}

#endif //OPEN_STREAM_ERROR_C
