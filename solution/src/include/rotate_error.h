#ifndef ROTATE_ERRORS_H
#define ROTATE_ERRORS_H
#include "inttypes.h"
#include "stdio.h"

enum rotate_status  {
    ROTATE_OK = 0,
    ROTATE_INVALID_ANGLE
};

enum rotate_status rotate_by_angle(int64_t angle, struct image* img, struct image* new_img );

static const char* rotate_errors[ROTATE_INVALID_ANGLE + 1] = {
        [ROTATE_OK] = "OK\n",
        [ROTATE_INVALID_ANGLE] = "Not correct angle\n"
};
#endif //ROTATE_ERRORS_H
