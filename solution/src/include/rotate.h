#ifndef ROTATE_H
#define ROTATE_H
#include <inttypes.h>

struct image* rotate_on_angle(struct image* const source, int64_t angle);
struct image rotate_0( struct image* const source, struct image* new);
struct image rotate_90( struct image* const source, struct image* new);
struct image rotate_180( struct image* const source, struct image* new);
struct image rotate_270( struct image* const source, struct image* new);

#endif //ROTATE_H
