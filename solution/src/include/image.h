#ifndef IMAGE_H
#define IMAGE_H
#include "header.h"
#include "pixel.h"
struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct image* load_image(const char* file_name);
int write_image(struct image const source, const char* file_name);
void free_image(struct image* img);
#endif //IMAGE_H
