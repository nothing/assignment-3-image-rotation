#ifndef OPEN_STREAM_ERROR_H
#define OPEN_STREAM_ERROR_H
#include "stdio.h"

enum open_stream_status{
    OPEN_OK = 0,
    OPEN_FAIL
};

enum open_stream_status open_stream(FILE* fp);

static const char* open_stream_errors[OPEN_FAIL + 1] = {
        [OPEN_OK] = "OK\n",
        [OPEN_FAIL] = "The stream could not be opened\n"
};
#endif //OPEN_STREAM_ERROR_H
