#ifndef INPUT_OUTPUT_H
#define INPUT_OUTPUT_H
#include "stdio.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    ALLOCATION_FAIL,
    READ_INVALID_PIXEL,
    SEEK_FAIL
};

enum read_status from_bmp( FILE* in, struct image* img);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

static const char* reading_errors[SEEK_FAIL + 1] = {
        [READ_OK] = "OK\n",
        [READ_INVALID_BITS] = "The number of bits per pixel is not supported\n",
        [READ_INVALID_HEADER] = "Some wrong with header\n",
        [READ_INVALID_PIXEL] = "Problems with pixels\n",
        [ALLOCATION_FAIL] = "Error happened during memory allocation\n",
        [SEEK_FAIL] = "Error happened during moving the pointer\n"
};

enum write_status to_bmp( FILE* out, struct image const* img, struct bmp_header* header);

static const char* writing_errors[WRITE_ERROR + 1] = {
        [WRITE_OK] = "OK\n",
        [WRITE_ERROR] = "Something went wrong during writing\n"
};
#endif //INPUT_OUTPUT_H
