#ifndef IO_ERRORS_C
#define IO_ERRORS_C
#define NUMBER_OF_BITS 24
#define INVALID_BITS 1
#define INVALID_HEADER 2
#define ALLOCATION_MEMORY_FAIL 3
#define INVALID_PIXEL 4
#define SEEK_MEMORY_FAIL 5
#define OK 0
#define ERROR_IN_WRITE 1
#include "include/image.h"
#include "include/input_output.h"
#include "inttypes.h"
#include "stdio.h"
#include "stdlib.h"

enum read_status from_bmp( FILE* in, struct image* img){
    if(img == NULL){
        printf("%s", reading_errors[ALLOCATION_MEMORY_FAIL]);
        return ALLOCATION_FAIL;
    }
    struct bmp_header header;
    uint8_t counter = fread(&header, sizeof(struct bmp_header), 1, in);
    if( counter < 1){
        printf("%s", reading_errors[INVALID_HEADER]);
        return READ_INVALID_HEADER;
    }
    if(header.biBitCount != NUMBER_OF_BITS){
        printf("%s", reading_errors[INVALID_BITS]);
        return READ_INVALID_BITS;
    }
    img->height = header.biHeight;
    img->width = header.biWidth;
    uint8_t padding = (4 - img->width * 3 % 4) % 4;
    img->data = (struct pixel*)malloc(sizeof(struct pixel) * img->width * img->height);
    if(fseek(in, (long)header.bOffBits, SEEK_SET) == -1){
        printf("%s", reading_errors[SEEK_MEMORY_FAIL]);
        return SEEK_FAIL;
    }
    for(size_t i = 0; i < img->height; ++i){
        if(fread(&img->data[(img->width)*i], sizeof(struct pixel), img->width, in) < img->width){
            printf("%s", reading_errors[INVALID_PIXEL]);
            return READ_INVALID_PIXEL;
        }
        if(fseek(in, padding, SEEK_CUR) == -1){
            printf("%s", reading_errors[SEEK_MEMORY_FAIL]);
            return SEEK_FAIL;
        }
    }
    printf("%s", reading_errors[OK]);
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img, struct bmp_header* header){
    uint8_t padding = (4 - img->width * 3 % 4) % 4;
    uint8_t bitmapfileheader = sizeof(header->bfReserved) + sizeof(header->bfType) + sizeof(header->bOffBits) + sizeof(header->bfileSize);
    header->bfType = 19778;
    header->bfileSize = (img->width * 3 + padding) * img->height + 54;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = sizeof(struct bmp_header) - bitmapfileheader;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = 1;
    header->biBitCount = sizeof(struct pixel) * 8;
    header->biCompression = 0;
    header->biSizeImage = (img->width * 3 + padding) * img->height;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    if(fwrite(header, sizeof(struct bmp_header), 1, out) < 1){
        printf("%s", writing_errors[ERROR_IN_WRITE]);
        return WRITE_ERROR;
    }
    for(size_t i = 0; i < img->height; ++i){
        if(fwrite(&img->data[(img->width)*i], sizeof(struct pixel), img->width, out) < img->width){
            printf("%s", writing_errors[ERROR_IN_WRITE]);
            return WRITE_ERROR;
        }
        for(uint8_t g = 0; g < padding; ++g){
            if(putc(0, out) == EOF){
                printf("%s", writing_errors[ERROR_IN_WRITE]);
                return WRITE_ERROR;
            }
        }

    }
    printf("%s", writing_errors[OK]);
    return WRITE_OK;
}

#endif//IO_ERRORS_C
