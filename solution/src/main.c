#include "include/image.h"
#include "include/rotate.h"
#include "stdlib.h"

int main( int argc, char** argv ) {
    if(argc < 4){
        exit(0);
    }
    int64_t arg = strtoll(argv[3], NULL, 10);
    struct image* img = load_image(argv[1]);
    struct image* new_img = rotate_on_angle(img,  arg);
    write_image(*new_img, argv[2]);
    free_image(new_img);
    free_image(img);
    return 0;
}
